#! /bin/sh

# use git archive recursively, gathers a versioned tree into
# $package-$version.tar.gz

usage(){
    echo "Usage: $0 <version-number> <path_for_archive>"
    echo
    echo "    Will call git archive resursively for version <version-number>"
    echo "    in the main repository, and default version for submodules"
    echo "    and build an archive named $package-<version>.tar.gz in"
    echo "    the path <path_for_archive>"
}

if [ $# -lt 2 ]; then
    usage
    exit 0
fi

version=$1
tag=$(echo -n "Version-"; echo $1)
if [ -z "$(git tag | grep ^$tag\$)" ]; then
    echo "the tag $tag does not exist. Stopping there."
    exit 0
fi

path=$2
package=supercollider
tarfile=$(realpath ${path}/${package}-${version}.tar)

rm -f ${tarfile}.xz

echo "Working with $tarfile"
git archive --format tar -o ${tarfile} ${tag}
echo "made ${tarfile}"
git submodule --quiet foreach --recursive 'git archive --format tar --prefix=$displaypath/ -o submodule.tar HEAD'
echo "archived submodules"
git submodule --quiet foreach --recursive "tar --concatenate --file=${tarfile} submodule.tar; rm -f submodule.tar"
echo -n "merged submodules; compressing... "
xz ${tarfile}
echo "[Done]"
echo "Made ${tarfile}.xz"
exit 0
